<!DOCTYPE html>
<html lang="en"> 
<head>
	<meta charset="utf-8"/>
	<title>q02_sol</title>

</head>

<body>
<?php

/*
* INFO/CS 1300
* Fall 2016
*
* Assignment 8, question 2
*
* Megan Tice
*/

// variables
$i; // counter variable
$j; // counter variable
$k; // counter variable
$temp_num; // holder for random value
$number_array = []; // array of random integers 1-5
$high_array = []; // integers from $number_array greater than 3


function make_randoms($temp_array){
	for ($i = 0; $i<=10; $i++) {
		$temp_num = rand(1, 5);
		array_push($temp_array, $temp_num);
	}
	print "$temp_array";
}


function get_and_sort($temp_array){
  $high_array = make_randoms($temp_array);
	for ($k=0; $k<=sizeof($high_array)-1; $k++){
	  if ($high_array[$k] > 3){
      echo "element: $high_array[$k]";
    }
	print "$high_array";
  }

get_and_sort($number_array); //send the empty array to get_and_sort
}

?>
	
</body>
</html>

