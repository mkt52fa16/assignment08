<!DOCTYPE html>

<html lang="en">
    <head>
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <meta charset="utf-8">
        <title>How to Properly Set a Table</title>
    </head>
    
    <body>
        <?php include 'includes/header.php';?>
        
        <div id="content">

            <div class=paragraphs>
                <p id="left_column">When walking into a restaurant, one can immediately tell the quality of the dining that  they are about to experience. This can be due to the mood lighting, music, atmosphere, and many other factors. However, one of the factors that people often do not notice that heavily impacts the atmosphere of the restaurant is the table setting. The number of silverware, plates and glasses immediately suggests the quality of food that is to be served, as well as how many courses those dining in the restaurant will be receiving.</p>
            
                <p id="right_column">Many individuals who approach a dinner table that is set with multiple forks and plates do not know which is to be used for which part of the meal. Their uses are a mystery, as well as why and how they are placed in a certain order. It is important to learn how to set a table properly, as which silverware, plate or glass to use at which part of the meal in order to impress your date or boss on those fancy dinner occasions that you are invited to.</p>
            </div><!-- end of paragraphs div -->
            
            <div class="images">
                <h2>Examples of Table Settings</h2>
                
                <figure> 
                    <img src="images/table_setting.jpg" alt="example_one"/>
                        <!-- image courtesy of House Beautiful http://www.housebeautiful.com/entertaining/table-decor/tips/g3619/table-setting-ideas/ Copyright 2016 HEARST COMMUNICATIONS, INC.  -->
                        <figcaption>This setting is just one of the many styles that can be used. The addition of the plant on the plate is used by the host in order to add a playful and unique touch to their table design. </figcaption>
                </figure>
            
                <figure>
                    <img src="images/table_two.jpg" alt="example_two"/>
                        <!-- image courtesy of Camille Styles http://camillestyles.com/summer/fave-outtakes-southern-wedding-shoot/ -->
                        <figcaption>This setting includes personalized name cards on top of the dishes, while remaining formal in style.</figcaption>
                </figure>
                
                <figure>
                        <figcaption>The following two images are much more informal in nature, with different style plates being used for each setting.</figcaption>
                        <img src="images/table_three.jpg" alt="example_three"/>
                        <!-- image courtesy of The Original Thanksgiving http://www.thanksgiving.com/living/decor/4-rustic-chic-table-setting-ideas-thanksgiving/ -->
            
                        <img src="images/table_four.jpg" alt="example_four"/>
                        <!-- image courtesy of Ballard Designs http://www.howtodecorate.com/2015/02/20-place-setting-ideas-for-your-table/ -->
                </figure>  
            </div> <!-- end of image div -->
                
        </div> <!-- end of content div -->
     </body>
</html>